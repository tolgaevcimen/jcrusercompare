package com.utils;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

public enum Publisher {
    ONE("01"),
    TWO("02"),
    THREE("03"),
    FOUR("04"),
    FIVE("05"),
    SIX("06"),
    SEVEN("07");

    private static final String URL_TEMPLATE = "http://aempub%s.abanet.org:4503/";
    private String CREDENTIALS;

    private String name;
    private Query query;

    Publisher(String name) {
        this.name = name;
    }

    public String getUrl() {
        return String.format(URL_TEMPLATE, name);
    }

    public String getCredentials() {
        return CREDENTIALS;
    }

    public Query getQuery() {
        return query;
    }

    public String getName() {
        return name;
    }

    public void initialize() {
        query = new Query(this);
        CREDENTIALS = Base64.getEncoder()
                .encodeToString((ModeSelector.getInstance().getJcrUsername()+":"+ModeSelector.getInstance().getJcrPassword())
                    .getBytes(StandardCharsets.UTF_8));
    }
}
