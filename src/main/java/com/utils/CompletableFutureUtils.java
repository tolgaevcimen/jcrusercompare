package com.utils;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public class CompletableFutureUtils {

    /**
     *
     * @param completableFutureList
     * @param interval Interval to check the completableFutureList to see if they all are done, in milliseconds
     */
    public static void waitUntillAllDone(List<CompletableFuture> completableFutureList, long interval) {
        while (true) {
            if (completableFutureList.stream().allMatch(CompletableFuture::isDone)) {
                return;
            } else {
                try {
                    Thread.sleep(interval);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
