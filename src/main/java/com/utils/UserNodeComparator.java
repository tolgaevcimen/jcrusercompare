package com.utils;

import com.models.UserNodeList;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;

public class UserNodeComparator {
    /**
     * Compares all users between publishers (users to be compared are picked by authorizableId)
     * @param publisherUserNodeListMap
     * @param executorService
     */
    public static void compareUserNodesAcrossAllServers(Map<Publisher, UserNodeList> publisherUserNodeListMap, ExecutorService executorService) {
        List<Publisher> publishers = new ArrayList<>(publisherUserNodeListMap.keySet());
        List<CompletableFuture> completableFutureList = new ArrayList<>();

        for(Publisher publisherX : publishers) {
            UserNodeList publisherXNodeList = publisherUserNodeListMap.get(publisherX);
            publisherXNodeList.forEach((authorizableId, userFromPublisher1) -> {
                for (Publisher publisherY : publishers) {
                    // don't compare with itself
                    if(publisherX.getName().equals(publisherY.getName())) continue;

                    JSONObject userFromPublisherY = publisherUserNodeListMap.get(publisherY).getWithKey(authorizableId);
                    // compare with multithreading
                    CompletableFuture<Void> completableFuture = CompletableFuture.runAsync(() ->
                            UserNodeComparator.compareUserNodes(userFromPublisher1, publisherX, userFromPublisherY, publisherY), executorService);
                    completableFutureList.add(completableFuture);
                }
            });
        }

        CompletableFutureUtils.waitUntillAllDone(completableFutureList, 10000);
    }

    /**
     * This method compares 2 user nodes from 2 publishers
     * @param mainObject
     * @param mainPub
     * @param objectToCompare
     * @param pubToCompare
     */
    public static void compareUserNodes(JSONObject mainObject, Publisher mainPub, JSONObject objectToCompare, Publisher pubToCompare) {
        // we compare these preselected 2 subnodes and their contents
        String[] nodeNamesToCompare = {"profile", "aba-cms-dotorg"};
        // we get profile subnode from first object as it is a must
        JSONObject mainProfile = mainObject.getJSONObject("profile");
        // we get authorizableId and email from the profile node we just extracted
        String mainProfileAuthorizableId = mainObject.getString("rep:authorizableId");
        String mainProfileEmail = mainProfile.getString("email");

        // flag to know if any subnode is different - if so we will check the customerData.xml
        boolean somethingDifferent = false;
        // iterating over the nodeNames to compare
        for (String nodeName : nodeNamesToCompare) {
            JSONObject mainNodeJson = mainObject.getJSONObject(nodeName);
            JSONObject objectToCompareJson = objectToCompare.getJSONObject(nodeName);
            // iterating over the subnodes
            for (String key : mainNodeJson.keySet()) {
                // if the other node does not have it, we report it as missing
                if (!objectToCompareJson.has(key)) {
                    Reporter.getInstance().report(
                            String.format("%s\t%s\tmissing field\t%s.%s\tpub%s \t'(pub%s has value: %s)'",
                                    mainProfileAuthorizableId, mainProfileEmail, nodeName, key,
                                    pubToCompare.getName(),
                                    mainPub.getName(), getFieldAsString(mainNodeJson, key)));
                    somethingDifferent = true;
                }
                // otherwise if the nodes have unequal values, we report it as different
                else if ( !mainNodeJson.get(key).equals(objectToCompareJson.get(key))) {
                    Reporter.getInstance().report(
                            String.format("%s\t%s\tdifferent field\t%s.%s\t\t'(pub%s: %s)!=(pub%s: %s)'",
                                    mainProfileAuthorizableId, mainProfileEmail, nodeName, key,
                                    mainPub.getName(), mainNodeJson.get(key).toString(),
                                    pubToCompare.getName(), getFieldAsString(objectToCompareJson, key)));
                    somethingDifferent = true;
                }
            }
        }

        // Because almost all nodes have this field different, the output size gets enourmous, hence we have put this report behind this guard
        if(ModeSelector.getInstance().shouldCheckCustomerDataSize()) {
            // we check the size property of customerData.Xml (because we can tell it from bulk query response, and it can help identifying bad customerData.xml's
            compareCustomerDataXmlSize(mainObject, mainPub, objectToCompare, pubToCompare, mainProfileAuthorizableId, mainProfileEmail);
        }

        // Comparing customerData.xml if something is different - and parameter is given by the client
        // (ideally we should always be checking it but as it's not coming from the bulk query and
        // it's a very heavy load to pull each time, so we only pull it when any of the other fields is different)
        if(somethingDifferent && ModeSelector.getInstance().shouldCheckCustomerData()) {
            compareCustomerData(mainPub, pubToCompare, mainProfileAuthorizableId, mainProfileEmail);
        }
    }

    /**
     * Gets the field value as a string - except bio (because it messes up the output - too long, html text)
     * @param object
     * @param key
     * @return
     */
    private static String getFieldAsString(JSONObject object, String key) {
        if(key == "bio") {
            return "bio truncated";
        }
        String value = object.get(key).toString();
        return value;
    }

    /**
     * Retrieves and compares customerData.xml from two publishers ignoring some unicodes (\r\n)
     * @param publisherX
     * @param publisherY
     * @param authorizableId
     * @param email
     */
    private static void compareCustomerData(Publisher publisherX, Publisher publisherY, String authorizableId, String email) {
        // TODO: as this is going to be done for each comparison with other publishers, customerData.xml's can be collected into a map,
        // TODO: and retrieved back without going to server - memory restrictions should be considered
        String customerDataXmlFromPublisherX = publisherX.getQuery().getCustomerDataXml(authorizableId);
        String customerDataXmlFromPublisherY = publisherY.getQuery().getCustomerDataXml(authorizableId);
        String var1 = "&#xD;&#xA;", var2 = "&#xD;", var3 = "&#xA;";

        customerDataXmlFromPublisherX = customerDataXmlFromPublisherX
                .replace(var1, System.lineSeparator())
                .replace(var2, System.lineSeparator())
                .replace(var3, System.lineSeparator());

        customerDataXmlFromPublisherY = customerDataXmlFromPublisherY
                .replace(var1, System.lineSeparator())
                .replace(var2, System.lineSeparator())
                .replace(var3, System.lineSeparator());

        if(!customerDataXmlFromPublisherX.equals(customerDataXmlFromPublisherY)) {
            Reporter.getInstance().report(
                    String.format("%s\t%s\tdifferent field\t%s\t\t'(pub%s)!=(pub%s)'",
                            authorizableId, email, "customerData.xml",
                            publisherX.getName(),
                            publisherY.getName()));
        }
    }

    /**
     * Extracts and compares customerDataXml nodes :jcr:data (size) attribute and reports if different
     * @param mainObject
     * @param mainPub
     * @param objectToCompare
     * @param pubToCompare
     * @param authId
     * @param email
     */
    private static void compareCustomerDataXmlSize(JSONObject mainObject, Publisher mainPub, JSONObject objectToCompare, Publisher pubToCompare, String authId, String email) {
        if(mainObject.has("customerData.xml") && objectToCompare.has("customerData.xml")) {
            JSONObject mainCustomerDataXml = mainObject.getJSONObject("customerData.xml");
            JSONObject otherCustomerDataXml = objectToCompare.getJSONObject("customerData.xml");
            if(mainCustomerDataXml.has("jcr:content") && otherCustomerDataXml.has("jcr:content")) {
                JSONObject mainJcrContent = mainCustomerDataXml.getJSONObject("jcr:content");
                JSONObject otherJcrContent = otherCustomerDataXml.getJSONObject("jcr:content");
                if(mainJcrContent.has(":jcr:data") && otherJcrContent.has(":jcr:data")) {
                    long mainJcrDataLength = mainJcrContent.getLong(":jcr:data");
                    long otherJcrDataLength = otherJcrContent.getLong(":jcr:data");
                    if(mainJcrDataLength != otherJcrDataLength) {
                        Reporter.getInstance().report(
                            String.format("%s\t%s\tdifferent field\t%s.%s\t\t'(pub%s: %s)!=(pub%s: %s)'",
                                authId, email, "customerData.xml", "Size()",
                                mainPub.getName(), mainJcrDataLength,
                                pubToCompare.getName(), otherJcrDataLength));
                    }
                }
            }
        }
    }
}
