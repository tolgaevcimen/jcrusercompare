package com.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

/**
 * This class handles the reportings
 */
public class Reporter {
    private static final String ERROR_FILE_PATH = "logs/error.log";
    private static final String REPORT_FILE_PATH = "logs/report.log";
    private static final String DUPLICATE_EMAIL_FILE_PATH = "logs/duplicates-on-pub%s-by-email.log";
    private static final String DUPLICATE_ABAID_FILE_PATH = "logs/duplicates-on-pub%s-by-abaId.log";
    private static final String BACKUP = "-backup";
    private static final String LINE_SEPARATOR = System.getProperty("line.separator");

    private static Reporter instance;
    private final File errorOutput;
    private final File reportOutput;
    private final File duplicatesByEmailOutput;
    private final File duplicatesByAbaIdOutput;


    private Reporter(File errorOutput, File reportOutput, File duplicatesByEmailOutput, File duplicatesByAbaIdOutput) {
        this.errorOutput = errorOutput;
        this.reportOutput = reportOutput;
        this.duplicatesByEmailOutput = duplicatesByEmailOutput;
        this.duplicatesByAbaIdOutput = duplicatesByAbaIdOutput;
    }

    public static void initialize(ModeSelector modeSelector) throws IOException {
        File errorFile = getFile(ERROR_FILE_PATH);
        File reportFile = getFile(REPORT_FILE_PATH);

        // if publisher is provided duplicate checker report files will be created with publishers name in it
        if(modeSelector.getPublisher() != null) {
            File duplicatesByEmailFile = getFile(String.format(DUPLICATE_EMAIL_FILE_PATH, modeSelector.getPublisher().getName()));
            File duplicatesByAbaIdFile = getFile(String.format(DUPLICATE_ABAID_FILE_PATH, modeSelector.getPublisher().getName()));
            instance = new Reporter(errorFile, reportFile, duplicatesByEmailFile, duplicatesByAbaIdFile);
        } else {
            instance = new Reporter(errorFile, reportFile, null, null);
        }
    }

    private static File getFile(String path) throws IOException {
        File file = new File(path);
        if (file.exists()) backUpFile(file);
        file.getParentFile().mkdirs();
        file.createNewFile();
        return file;
    }

    private static void backUpFile(File file) {
        boolean result = file.renameTo(new File(file.getPath() + BACKUP + System.nanoTime()));

        if (!result) throw new RuntimeException("Error while backup operation for file: " + file.getPath());
        file.delete();
    }

    public static Reporter getInstance() {
        if (instance == null) throw  new RuntimeException("com.utils.Reporter is not initialized");
        else return instance;
    }

    public void report(String message) {
        appendToFile(reportOutput, message + LINE_SEPARATOR);
    }

    public void error(String message) {
        appendToFile(errorOutput, message + LINE_SEPARATOR);
    }

    public void duplicatesByEmail(String message) {
        appendToFile(duplicatesByEmailOutput, message + LINE_SEPARATOR);
    }

    public void duplicatesByAbaId(String message) {
        appendToFile(duplicatesByAbaIdOutput, message + LINE_SEPARATOR);
    }

    private void appendToFile(File file, String message) {
        try {
            Files.write(Paths.get(file.getPath()), message.getBytes(), StandardOpenOption.APPEND);
        } catch (IOException e) {
            System.err.println("Could not write to file " + file.getPath());
        }

    }
}
