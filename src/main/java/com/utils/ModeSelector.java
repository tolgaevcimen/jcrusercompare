package com.utils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ModeSelector {
    private static final String MODE = "--mode";
    private static final String PUBLISHER = "--publisher";
    private static final String PUBLISHERS = "--publishers";
    private static final String CUSTOMERDATA_CHECK = "--checkCustomerData";
    private static final String CUSTOMERDATASIZE_CHECK = "--checkCustomerDataSize";

    private static final String TAKE = "--take";
    private static final String TIMEOUT = "--timeout";

    private static final String JCR_USERNAME = "--jcrUser";
    private static final String JCR_PASSWORD = "--jcrPass";

    private static ModeSelector instance;
    private Mode mode;
    private Publisher publisher;
    private Publisher[] publishers;
    private boolean checkCustomerData;
    private boolean checkCustomerDataSize;
    private int take;
    private int timeout;
    private String jcrUsername;
    private String jcrPassword;

    private ModeSelector(Mode mode, Publisher publisher, Publisher[] publishers, boolean checkCustomerData, boolean checkCustomerDataSize, int take, int timeout, String jcrUsername, String jcrPassword) {
        this.mode = mode;
        this.publisher = publisher;
        this.publishers = publishers;
        this.checkCustomerData = checkCustomerData;
        this.checkCustomerDataSize = checkCustomerDataSize;
        this.take = take;
        this.timeout = timeout;
        this.jcrUsername = jcrUsername;
        this.jcrPassword = jcrPassword;
    }

    public static void initialize(Map<String, String> commandLineParams) {
        String modeParam = commandLineParams.get(MODE);
        Mode runMode = modeParam == null ? Mode.UNKNOWN : modeParam.equals("1") ? Mode.FULL_COMPARISON : modeParam.equals("2") ? Mode.DUPLICATES : Mode.UNKNOWN;

        String publisherName = commandLineParams.get(PUBLISHER);
        Publisher publisher = publisherName == null ? null : Publisher.valueOf(publisherName);

        String publishersNames = commandLineParams.get(PUBLISHERS);
        Publisher[] publishers = extractPublishers(publishersNames);

        String checkCustomerDataParam = commandLineParams.get(CUSTOMERDATA_CHECK);
        boolean checkCustomerData = Boolean.valueOf(checkCustomerDataParam);

        String checkCustomerDataSizeParam = commandLineParams.get(CUSTOMERDATASIZE_CHECK);
        boolean checkCustomerDataSize = Boolean.valueOf(checkCustomerDataSizeParam);

        String takeParam = commandLineParams.get(TAKE);
        int take = takeParam != null ? Integer.valueOf(takeParam) : 500;

        String timeoutParam = commandLineParams.get(TIMEOUT);
        int timeout = timeoutParam != null ? Integer.valueOf(timeoutParam) : 30;

        String jcrUsernameParam = commandLineParams.get(JCR_USERNAME);
        String jcrUsername = jcrUsernameParam != null ? jcrUsernameParam : "bill@eresources.com";

        String jcrPasswordParam = commandLineParams.get(JCR_PASSWORD);
        String jcrPassword = jcrPasswordParam != null ? jcrPasswordParam : "C0mm1ss10n";

        instance = new ModeSelector(runMode, publisher, publishers, checkCustomerData, checkCustomerDataSize, take, timeout, jcrUsername, jcrPassword);
    }

    /**
     * Extracts publishers from parameters, if not provided then returns all Publishers
     * @param publishers
     * @return
     */
    private static Publisher[] extractPublishers(String publishers) {
        try {
            if (publishers != null) {
                String[] publisherNames = publishers.split(",");
                List<Publisher> _publishers = Arrays.stream(publisherNames).map(Publisher::valueOf).collect(Collectors.toList());
                Publisher[] publisherArray = new Publisher[_publishers.size()];
                for (int i = 0; i < _publishers.size(); i++) {
                    publisherArray[i] = _publishers.get(i);
                }
                return publisherArray;
            }
            return Publisher.values();
        } catch (Exception e) {
            System.out.println("Please use the correct format for publishers. eg. ONE,THREE,FOUR");
            throw e;
        }
    }

    public static ModeSelector getInstance() {
        if (instance == null) throw  new RuntimeException("com.utils.ModeSelector is not initialized");
        else return instance;
    }

    public Mode getMode() {
        return mode;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public boolean shouldCheckCustomerData() {
        return checkCustomerData;
    }

    public boolean shouldCheckCustomerDataSize() {
        return checkCustomerDataSize;
    }
    public int getTake() {
        return take;
    }

    public int getTimeout() {
        return timeout;
    }

    public String getJcrUsername() {
        return jcrUsername;
    }

    public String getJcrPassword() {
        return jcrPassword;
    }

    public Publisher[] getPublishers() {
        return publishers;
    }

    public enum Mode {
        FULL_COMPARISON,
        DUPLICATES,
        UNKNOWN
    }
}
