package com.utils;

import com.jcabi.aspects.RetryOnFailure;
import com.models.UserNodeList;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.ResponseBody;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * This class instantiated with a publisher, hence every request is made to the relevant publisher with its credentials
 */
public class Query {
    private Publisher publisher;
    private int take;
    private int timeout;

    public Query(Publisher publisher) {
        this.publisher = publisher;
        this.take = ModeSelector.getInstance().getTake();
        this.timeout = ModeSelector.getInstance().getTimeout();
    }

    /**
     * Gets user count in given path
     * @param path
     * @return
     */
    public int getUserCountInPath(String path) {
        JSONObject jsonResponse = getJsonResponse(publisher.getUrl() + getUserNodeQueryString(path, 0, 1), publisher.getCredentials());
        return jsonResponse.getInt("total");
    }

    /**
     * Gets all users under given path (method that collects users in a bulky manner)
     * @param path
     * @param total
     * @param executorService
     * @return
     */
    public UserNodeList getAllUserNodesInPath(String path, int total, ExecutorService executorService) {
        List<Supplier<JSONArray>> suppliers = getUserNodeSuppliersWithPath(path, total);
        Map<String, JSONObject> authorizableIdJSONObjectMap = consumeSuppliers(executorService, suppliers);
        return new UserNodeList(authorizableIdJSONObjectMap);
    }

    /**
     * Retrieves customerData.xml given the authorizableId
     * @param authorizableId
     * @return
     */
    public String getCustomerDataXml(String authorizableId) {
        try {
            return getStringResponseWithOKHttp(publisher.getUrl() + getCustomerDataXmlQueryString(authorizableId), publisher.getCredentials());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    /**
     * Gets all folder under /home/users
     * @return
     */
    public Set<String> getAllUserFolderPaths() {
        JSONObject result = getJsonResponse(publisher.getUrl() + "/home/users.1.json", publisher.getCredentials());
        return result.keySet()
                .stream()
                .filter(s -> !s.startsWith("jcr:") && !s.startsWith("rep:"))
                .map(s -> "/home/users/" + s )
                .collect(Collectors.toSet());
    }

    /**
     * Makes the http GET query to the given url, retries on failure after a 1ms delay
     * @param url
     * @param credentials
     * @return body as string
     * @throws IOException
     */
    @RetryOnFailure(attempts = 10, delay = 1)
    private String getStringResponseWithOKHttp(String url, String credentials) throws IOException {
        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(this.timeout, TimeUnit.SECONDS);
        client.setReadTimeout(this.timeout, TimeUnit.SECONDS);

        Request request = new Request.Builder()
                .url(url)
                .get()
                .addHeader("Authorization", "Basic " + credentials)
                .build();

        Response response = client.newCall(request).execute();
        try (ResponseBody body = response.body()) {
            return body.string();
        } catch (Exception e) {
            throw e;
        } finally {
            if (response != null && response.body() != null) {
                response.body().close();
            }
        }
    }

    /**
     * Makes the request to the given url, with given credentials, reports error if can't finish successfully
     * @param url
     * @param credentials
     * @return
     */
    private JSONObject getJsonResponse(String url, String credentials) {
        String response = null;
        try {
            response = getStringResponseWithOKHttp(url, credentials);
        } catch (IOException e) {
            System.out.printf("Tried below url for 10 times, still throws %s:\n%s%n", e.getMessage(), url);
        }
        return response == null ? null : new JSONObject(response);
    }

    /**
     * url for getting the user nodes paged and ordered
     * (notice that nodes are ordered by aba-cms-dotorg/company_location as it is one of the indexed fields)
     * @param parentNodePath
     * @param offset
     * @param limit
     * @return
     */
    private String getUserNodeQueryString(String parentNodePath, int offset, int limit) {
        return String.format("/bin/querybuilder.json?path=%s&p.limit=%s&orderby=@aba-cms-dotorg/company_location&orderby.sort=asc&type=rep:User&p.offset=%s&p.hits=full&p.nodedepth=2",
                parentNodePath, limit, offset);
    }

    /**
     * url for retrieving the customerData.xml of a user
     * @param authorizableId
     * @return
     */
    private String getCustomerDataXmlQueryString(String authorizableId) {
        return String.format("home/users/%s/%s/customerData.xml", authorizableId.charAt(0), authorizableId);
    }

    private Map<String, JSONObject> consumeSuppliers(ExecutorService executorService, List<Supplier<JSONArray>> userNodeSuppliers) {
        Map<String, JSONObject> authorizableIdJsonObjectMap = new HashMap<>();
        List<CompletableFuture> completableFutures = new ArrayList<>();
        userNodeSuppliers.forEach(supplier -> {
            CompletableFuture<Void> completableFuture = CompletableFuture.supplyAsync(supplier, executorService)
                    .thenAccept(getUserNodeArrayConsumer(authorizableIdJsonObjectMap));
            completableFutures.add(completableFuture);
        });

        CompletableFutureUtils.waitUntillAllDone(completableFutures, 10000);
        return authorizableIdJsonObjectMap;
    }

    private Consumer<JSONArray> getUserNodeArrayConsumer(Map<String, JSONObject> authorizableIdJsonObjectMap) {
        return jsonarray -> {
            for (int index = 0; index < jsonarray.length(); index++) {
                JSONObject jsonObject = jsonarray.getJSONObject(index);
                try {
                    String authorizableId = jsonObject.getString("rep:authorizableId");
                    synchronized (this) {
                        if (authorizableIdJsonObjectMap.containsKey(authorizableId)) {
                            Reporter.getInstance().error(String.format("Duplicate user found on publisher: %s for authorizableId: %s", publisher.getName(), authorizableId));
                        } else {
                            authorizableIdJsonObjectMap.put(authorizableId, jsonObject);
                        }
                    }
                } catch (Exception e) {
                    Reporter.getInstance().error(String.format("Error while reading user node: %s from publisher: %s", jsonObject.getString("jcr:path")));
                    e.printStackTrace();
                }
            }
        };
    }

    /**
     * @return Splits the total work of getting all user nodes from a publisher into a list of suppliers.
     */
    private List<Supplier<JSONArray>> getUserNodeSuppliersWithPath(String path, int total) {
        List<Supplier<JSONArray>> suppliers = new ArrayList<>();

        for (int index = 0; index < total; index += take) {
            suppliers.add(getUserNodeSupplier(path, take, index, total));
        }

        return suppliers;
    }

    /**
     *
     * @param userFolderPath parent userFolderPath path to search for users
     * @param take how many nodes to take
     * @param skip how many nodes to skip
     * @return Return a supplier which takes a list of nodes from the publisher and provides the result as a json array
     */
    private Supplier<JSONArray> getUserNodeSupplier(String userFolderPath, int take, int skip, int total) {
        return () -> {
            try {
                JSONObject userNodes = getJsonResponse(publisher.getUrl() + getUserNodeQueryString(userFolderPath, skip, take), publisher.getCredentials());
                if (userNodes != null && userNodes.getInt("results") > 0) {
                    System.out.printf("Gathered %d-%d/%d%n", skip, skip + take, total);
                    return userNodes.getJSONArray("hits");
                }
                else {
                    System.out.printf("Couldn't gather %d-%d/%d%n", skip, skip + take, total);
                    return null;
                }
            } catch (Exception e){
                System.out.println(e.getMessage());
                return null;
            }
        };
    }

    public Publisher getPublisher() {
        return publisher;
    }
}
