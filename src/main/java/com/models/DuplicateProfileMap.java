package com.models;

import org.json.JSONObject;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DuplicateProfileMap {
    private Map<String, ArrayList<AbstractMap.SimpleEntry<String, String>>> duplicateProfilesByEmailList;
    private Map<String, ArrayList<AbstractMap.SimpleEntry<String, String>>> duplicateProfilesByAbaIdList;

    public DuplicateProfileMap () {
        duplicateProfilesByEmailList = new HashMap<>();
        duplicateProfilesByAbaIdList = new HashMap<>();
    }

    private void put(Map<String, ArrayList<AbstractMap.SimpleEntry<String, String>>> map, String key, String authorizableId, String value) {
        ArrayList profilesForKey = map.get(key);
        if(profilesForKey == null) {
            profilesForKey = new ArrayList();
        }
        profilesForKey.add(new AbstractMap.SimpleEntry<>(authorizableId, value));
        map.put(key, profilesForKey);
    }

    public void put(String pubName, UserNodeList users) {
        for (Map.Entry<String, JSONObject> user : users.getJSONObjectMap().entrySet()) {
            JSONObject userNode = user.getValue();
            if(!userNode.has("profile")) {
                continue;
            }
            JSONObject profile = userNode.getJSONObject("profile");
            String profileAbaId = "";
            if (profile.has("abaId")) {
                profileAbaId = profile.getString("abaId");
            }

            String email = "";
            if (profile.has("email")) {
                email = profile.getString("email");
            }

            String cmsDotOrgAbaId = "";
            if(userNode.has("aba-cms-dotorg")) {
                JSONObject cmsDotorg = userNode.getJSONObject("aba-cms-dotorg");
                if (cmsDotorg.has("abaId")) {
                    cmsDotOrgAbaId = cmsDotorg.getString("abaId");
                }
            }

            String modifiedDate = "";
            if(userNode.has("customerData.xml")) {
                JSONObject customerData = userNode.getJSONObject("customerData.xml");
                if (customerData.has("jcr:content")) {
                    JSONObject content = customerData.getJSONObject("jcr:content");
                    if(content.has("jcr:lastModified")) {
                        modifiedDate = content.getString("jcr:lastModified");
                    }
                }
            }

            put(duplicateProfilesByEmailList, email + "\tpub" + pubName, user.getKey(), profileAbaId + "\t" + cmsDotOrgAbaId + "\t" + modifiedDate );
            put(duplicateProfilesByAbaIdList, profileAbaId + "\tpub" + pubName, user.getKey(), email + "\t" + modifiedDate);
        }
    }

    private Map<String, ArrayList<AbstractMap.SimpleEntry<String, String>>> getDuplicates(Map<String, ArrayList<AbstractMap.SimpleEntry<String, String>>> map) {
        Map<String, ArrayList<AbstractMap.SimpleEntry<String, String>>> duplicates = new HashMap<>();
        for (Map.Entry<String, ArrayList<AbstractMap.SimpleEntry<String, String>>> profileOnPublisher : map.entrySet()) {
            if(profileOnPublisher.getValue().size() > 1) {
                duplicates.put(profileOnPublisher.getKey(), profileOnPublisher.getValue());
            }
        }
        return duplicates;
    }

    public Map<String, ArrayList<AbstractMap.SimpleEntry<String, String>>> getDuplicatesByEmail() {
        return getDuplicates(duplicateProfilesByEmailList);
    }

    public Map<String, ArrayList<AbstractMap.SimpleEntry<String, String>>> getDuplicatesByAbaId() {
        return getDuplicates(duplicateProfilesByAbaIdList);
    }
}
