package com.models;

import org.json.JSONObject;

import java.util.Map;
import java.util.function.BiConsumer;

public class UserNodeList {

    private Map<String, JSONObject> JSONObjectMap;

    public UserNodeList(Map<String, JSONObject> JSONObjectMap) {
        this.JSONObjectMap = JSONObjectMap;
    }

    public void forEach(BiConsumer<String, JSONObject> callback) {
        JSONObjectMap.forEach(callback);
    }

    public JSONObject getWithKey(String key) {
        return JSONObjectMap.get(key);
    }

    public Map<String, JSONObject> getJSONObjectMap() {
        return JSONObjectMap;
    }
}
