package com;

import com.JCRProcessors.DuplicateChecker;
import com.JCRProcessors.FolderBasedBulkComparator;
import com.utils.ModeSelector;
import com.utils.Publisher;
import com.utils.Reporter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Main {

    /**
     * Entry point of the program
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        long start=System.nanoTime();
        initializeModeSelector(args);
        initializePublishers();
        Reporter.initialize(ModeSelector.getInstance());

        // According to the requested mode, we start the process (Duplication Check or Full Comparison)
        switch (ModeSelector.getInstance().getMode()) {
            case DUPLICATES:
                System.out.println("Started duplication check");
                DuplicateChecker duplicateChecker = new DuplicateChecker(ModeSelector.getInstance().getPublisher());
                duplicateChecker.run();
                System.out.println("All Finished in " + ((double) (System.nanoTime()-start) / 1_000_000_000 / 60) + "min");
                break;
            case FULL_COMPARISON:
                System.out.println("Started full comparison");
                FolderBasedBulkComparator bulkComparator = new FolderBasedBulkComparator();
                bulkComparator.run();
                System.out.println("All Finished in " + ((double) (System.nanoTime()-start) / 1_000_000_000 / 60) + "min");
                break;
            default:
                System.out.println("Please provide a valid mode with --mode, 1 (Full Comparison) or 2 (Duplicate Check)");
                break;
        }
    }

    private static void initializeModeSelector(String[] args) {
        Map<String, String> commandLineParams = new HashMap<>();
        for (int index = 0; index < args.length; index += 2) {
            commandLineParams.put(args[index], args[index+1]);
        }

        ModeSelector.initialize(commandLineParams);
    }

    private static void initializePublishers() {
        for (Publisher publisher : Publisher.values()) {
            publisher.initialize();
        }
    }
}
