package com.JCRProcessors;

import com.utils.ModeSelector;
import com.utils.Publisher;
import com.utils.Reporter;
import com.utils.UserNodeComparator;
import com.models.UserNodeList;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FolderBasedBulkComparator {
    ExecutorService executorService = Executors.newFixedThreadPool(10);

    public void run() {
        // Put titles as the first line to the report file
        Reporter.getInstance().report(String.format("authorizableId\temail\tdiff type\tfield\tmissing from pub\tdiff detail"));

        // Get all user paths from pub01 (pub01 is just selected for the sake of simplicity)
        // There are 24 paths under /home/users
        Set<String> allUserFolderPaths = Publisher.ONE.getQuery().getAllUserFolderPaths();
        int processedCount = 0;

        // Iterate over the paths
        for (String userFolderPath : allUserFolderPaths) {
            long start = System.nanoTime();
            System.out.printf("Collecting users in path: %s, progress: %d/%d%n", userFolderPath, processedCount++, allUserFolderPaths.size());
            Map<Publisher, UserNodeList> publisherUserNodeListMap = gatherUserNodesByPublisher(userFolderPath);

            System.out.println("Comparing nodes");
            UserNodeComparator.compareUserNodesAcrossAllServers(publisherUserNodeListMap, executorService);

            System.out.printf("Path Finished in %s min%n", (double) (System.nanoTime() - start) / 1_000_000_000 / 60);

            // Remove this when you want to do full comparision, here just for development purposes
//            return;
        }
    }

    /**
     * By giving the path to each publisher, collects all user nodes under that path, puts them in a map keyed by publisher
     * @param userFolderPath
     */
    private Map<Publisher, UserNodeList> gatherUserNodesByPublisher(String userFolderPath) {
        Map<Publisher, UserNodeList> publisherUserNodeListMap = new HashMap<>();

        for (Publisher publisher : ModeSelector.getInstance().getPublishers()) {
            System.out.printf("Collecting from pub%s%n", publisher.getName());
            long pubTime = System.nanoTime();
            try {
                // Get total user count under path
                int totalUserCountInPath = publisher.getQuery().getUserCountInPath(userFolderPath);

                // Retrieve all users under given folder
                UserNodeList users = publisher.getQuery().getAllUserNodesInPath(userFolderPath, totalUserCountInPath, executorService);

                System.out.printf("Gathered %d users out of %d from pub%s - path %s%n", users.getJSONObjectMap().size(), totalUserCountInPath, publisher.getName(), userFolderPath);

                // Put found users into map
                publisherUserNodeListMap.put(
                        publisher,
                        users
                );
            } catch (Exception e) {
                Reporter.getInstance().error("Error while getting nodes. Excluding publisher: " + publisher.getName());
                e.printStackTrace();
            }
            System.out.printf("Finished in %ss%n", (double) (System.nanoTime() - pubTime) / 1_000_000_000);
        }

        return publisherUserNodeListMap;
    }

}
