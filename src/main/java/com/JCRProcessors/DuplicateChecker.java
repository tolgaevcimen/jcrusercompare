package com.JCRProcessors;

import com.models.DuplicateProfileMap;
import com.models.UserNodeList;
import com.utils.ModeSelector;
import com.utils.Publisher;
import com.utils.Reporter;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DuplicateChecker {
    private Publisher publisher;
    public DuplicateChecker(Publisher publisher) {
        this.publisher = publisher;
    }

    public void run() {
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        long pubTime = System.nanoTime();

        // collect duplicate profiles against email and abaId separately
        DuplicateProfileMap duplicateProfiles = collectDuplicateProfiles(publisher, executorService);
        if(duplicateProfiles == null) return;

        // report duplicate profiles
        String message = ModeSelector.getInstance().shouldCheckCustomerData() ? "Reporting & Retrieving CustomerData.xml" : "Reporting";
        System.out.println(message);
        reportDuplicatesByEmail(duplicateProfiles.getDuplicatesByEmail());
        reportDuplicatesByAbaId(duplicateProfiles.getDuplicatesByAbaId());
        System.out.printf("Finished in %ss%n", (double) (System.nanoTime() - pubTime) / 1_000_000_000);
    }

    /**
     * Collects all profiles into DuplicateProfileMap
     * @param publisher
     * @param executorService
     * @return
     */
    private DuplicateProfileMap collectDuplicateProfiles(Publisher publisher, ExecutorService executorService) {
        if(publisher == null) {
            System.out.println("Please provide a publisher with --publisher, eg. ONE, TWO, THREE...");
            return null;
        }

        int processedCount = 0;
        DuplicateProfileMap duplicateProfiles = new DuplicateProfileMap();
        Set<String> allUserFolderPaths = publisher.getQuery().getAllUserFolderPaths();
        for (String userFolderPath : allUserFolderPaths) {
            long start=System.nanoTime();
            System.out.println("Collecting users in path: " + userFolderPath + ", progress: " + processedCount++ + "/" + allUserFolderPaths.size());

            System.out.println("Collecting from pub" + publisher.getName());
            try {
                int totalUserCountInPath = publisher.getQuery().getUserCountInPath(userFolderPath);
                UserNodeList users = publisher.getQuery().getAllUserNodesInPath(userFolderPath, totalUserCountInPath, executorService);
                System.out.printf("Gathered %d users out of %d from pub%s - path %s%n", users.getJSONObjectMap().size(), totalUserCountInPath, publisher.getName(), userFolderPath);

                duplicateProfiles.put(publisher.getName(), users);
            } catch (Exception e) {
                Reporter.getInstance().error("Error while getting nodes. Excluding publisher: " + publisher.getName());
                e.printStackTrace();
            }
            System.out.println("Path Finished in " + ((double) (System.nanoTime()-start) / 1_000_000_000 / 60) + "min");

            //Remove this when you want to do full check
//            break;
        }

        return duplicateProfiles;
    }

    private void reportDuplicatesByEmail(Map<String, ArrayList<AbstractMap.SimpleEntry<String, String>>> duplicates) {
        System.out.printf("found %d duplicate users by email%n", duplicates.entrySet().size());
        boolean checkCustomerDataXml = ModeSelector.getInstance().shouldCheckCustomerData();
        if(checkCustomerDataXml) {
            Reporter.getInstance().duplicatesByEmail(String.format("%s\t%s\t%s\t%s\t%s\t%s\t%s", "email", "publisher", "authorizableId", "profile.abaId", "aba-cms-dotorg.abaId", "modifiedDate", "createdUserExists"));
        } else {
            Reporter.getInstance().duplicatesByEmail(String.format("%s\t%s\t%s\t%s\t%s\t%s", "email", "publisher", "authorizableId", "profile.abaId", "aba-cms-dotorg.abaId", "modifiedDate"));
        }
        for (Map.Entry<String, ArrayList<AbstractMap.SimpleEntry<String, String>>> duplicate : duplicates.entrySet()) {
            String duplicateKeyOnPublisher = duplicate.getKey();
            for (AbstractMap.SimpleEntry<String, String> detail : duplicate.getValue()) {
                if(checkCustomerDataXml) {
                    String createdUserExists = getCreatedUserExists(detail.getKey());
                    Reporter.getInstance().duplicatesByEmail(String.format("%s\t%s\t%s\t%s", duplicateKeyOnPublisher, detail.getKey(), detail.getValue(), createdUserExists));
                } else {
                    Reporter.getInstance().duplicatesByEmail(String.format("%s\t%s\t%s", duplicateKeyOnPublisher, detail.getKey(), detail.getValue()));
                }
            }
        }
    }

    private void reportDuplicatesByAbaId(Map<String, ArrayList<AbstractMap.SimpleEntry<String, String>>> duplicates) {
        System.out.printf("found %d duplicate users by abaId%n", duplicates.entrySet().size());
        boolean checkCustomerDataXml = ModeSelector.getInstance().shouldCheckCustomerData();
        if(checkCustomerDataXml) {
            Reporter.getInstance().duplicatesByAbaId(String.format("%s\t%s\t%s\t%s\t%s\t%s", "abaId", "publisher", "authorizableId", "email", "modifiedDate", "createdUserExists"));
        } else {
            Reporter.getInstance().duplicatesByAbaId(String.format("%s\t%s\t%s\t%s\t%s", "abaId", "publisher", "authorizableId", "email", "modifiedDate"));
        }

        for (Map.Entry<String, ArrayList<AbstractMap.SimpleEntry<String, String>>> duplicate : duplicates.entrySet()) {
            String duplicateKeyOnPublisher = duplicate.getKey();
            for (AbstractMap.SimpleEntry<String, String> detail : duplicate.getValue()) {
                if(checkCustomerDataXml) {
                    String createdUserExists = getCreatedUserExists(detail.getKey());
                    Reporter.getInstance().duplicatesByAbaId(String.format("%s\t%s\t%s\t%s", duplicateKeyOnPublisher, detail.getKey(), detail.getValue(), createdUserExists));
                } else {
                    Reporter.getInstance().duplicatesByAbaId(String.format("%s\t%s\t%s", duplicateKeyOnPublisher, detail.getKey(), detail.getValue()));
                }
            }
        }
    }

    /**
     * customerData.xml contains a field <CreatedUserExists></CreatedUserExists> which we thought might be useful for duplicate handling, hence we check it
     * @param authorizableId
     * @return
     */
    private String getCreatedUserExists(String authorizableId) {

        String createdUserExists = null;
        try {
            String customerDataXml = publisher.getQuery().getCustomerDataXml(authorizableId).toLowerCase();
            if(customerDataXml.contains("<createduserexists>false</createduserexists>")) {
                createdUserExists = "false";
            } else if(customerDataXml.contains("<createduserexists>true</createduserexists>")) {
                createdUserExists = "true";
            }
        } catch (Exception e) {
            System.out.println("Couldn't process CustomerDataXml for " + authorizableId);
        }
        return createdUserExists;
    }
}
